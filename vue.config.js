const { defineConfig } = require("@vue/cli-service");
module.exports = defineConfig({
  transpileDependencies: false,
  devServer: {
    host: "127.0.0.1",
    port: "8081",
    open: true,
    proxy: {
      "/dev-api": {
        target: "http://gmall-h5-api.atguigu.cn/",
        changeOrigin: true,
        pathRewrite: {
          "^/dev-api": "",
        },
      },
    },
  },

  configureWebpack: {
    resolve: {
      alias: {
        "@comp": "@/components",
      },
    },
  },
});
