import Mock from "mockjs";
import banner from "./banner.json";
import floor from "./floor.json";

Mock.mock("/api/banner", "post", () => {
  return {
    code: 200,
    message: "ok",
    data: banner,
  };
});

Mock.mock("/api/floor", "post", () => {
  return {
    code: 200,
    message: "ok",
    data: floor,
  };
});
