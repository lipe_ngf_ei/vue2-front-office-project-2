import Vue from "vue";
import VueRouter from "vue-router";
import routes from "./routers";

Vue.use(VueRouter);

//重写push
const oldPush = VueRouter.prototype.push;

//push之所以会报错是因为push会返回一个promise却没有函数接收，只要给他写一个接收函数就可以了

VueRouter.prototype.push = function (
  location,
  onComplate = () => {},
  onAbout = () => {}
) {
 return oldPush.call(this, location, onComplate, onAbout);
};

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
