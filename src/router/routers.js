export default [
  {
    path: "/",
    redirect: "/home",
  },
  {
    path: "/home",
    name: "Home",
    component: () => import("@/pages/Home"),
  },
  {
    path: "/login",
    name: "Login",
    component: () => import("@/pages/Login"),
  },
  {
    path: "/register",
    name: "Register",
    component: () => import("@/pages/Register"),
  },
  {
    path: "/search",
    name: "Search",
    component: () => import("@/pages/Search"),
  },
  {
    path: "/404",
    name: "NotFound",
    component: () => import("@/pages/404"),
  },
  {
    path:'/*',
    redirect:"/404"
  }
];
