import axios from 'axios'

//创建axios实例
const request = axios.create({
  baseURL: process.env.VUE_APP_API,
  timeout:5000,
  headers:{}
})

//创建请求拦截器
request.interceptors.request.use(
  (config)=>{
    return config
  },
  (error)=>{
    return Promise.reject(error)
  }
)

//创建响应拦截器
request.interceptors.response.use(
  (res)=>{
    if(res.data.code !== 200){
      return Promise.reject(res.data)
    }
  return res.data.data
  },
  (error)=>{
    return Promise.reject(error)
  }
)

export default request