import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import Swiper from "@/components/swiper";
import "@/mock";
Vue.config.productionTip = false;

Vue.component("Swiper", Swiper);
new Vue({
  router,
  store,
  beforeCreate() {
    Vue.prototype.$bus = this;
  },
  render: (h) => h(App),
}).$mount("#app");
