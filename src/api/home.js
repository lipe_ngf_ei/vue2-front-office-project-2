import { requestMock } from "@/request/requestMock";

export const reqBannerList = () => {
  return requestMock.post(`/api/banner`);
};

export const reqFloorList = () => {
  return requestMock.post(`/api/floor`);
};
