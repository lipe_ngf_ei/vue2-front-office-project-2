import { request } from "@/request";

//获取一级分类列表
export const reqCategoryList1 = () => {
  return request.get(`/admin/product/getCategory1`);
};

//获取二级分类列表
export const reqCategoryList2 = (category1Id) => {
  return request.get(`/admin/product/getCategory2/${category1Id}`);
};

//获取三级分类列表
export const reqCategoryList3 = (category2Id) =>{
  return request.get(`/admin/product/getCategory3/${category2Id}`)
}
